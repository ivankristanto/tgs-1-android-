package com.example.ifunk.penghitungskripsi.Model;

public class KumpulanKriteria {
    private KriteriaPenilaianSkripsi tataTulis;
    private KriteriaPenilaianSkripsi kelengkapanMateri;
    private KriteriaPenilaianSkripsi poster;
    private KriteriaPenilaianSkripsi penguasaanMateri;
    private KriteriaPenilaianSkripsi prosesBimbingan;

    private float totalNilai;

    public KumpulanKriteria (){
        this.tataTulis = new KriteriaPenilaianSkripsi("Tata Tulis",0.15f);
        this.kelengkapanMateri = new KriteriaPenilaianSkripsi("Kelengkapan Materi",0.15f);
        this.poster = new KriteriaPenilaianSkripsi("Poster",0.10f);
        this.penguasaanMateri = new KriteriaPenilaianSkripsi("Penguasaan Materi",0.30f);
        this.prosesBimbingan = new KriteriaPenilaianSkripsi("Proses Bimbingan",0.30f);
    }

    public void hitungTotalNilai(){
        this.totalNilai = this.tataTulis.getNilai()+this.kelengkapanMateri.getNilai()+this.poster.getNilai()+this.penguasaanMateri.getNilai()+this.prosesBimbingan.getNilai();
    }


    public float getTotalNilai(){
        return this.totalNilai;
    }

    //Setter & Getter
    public KriteriaPenilaianSkripsi getTataTulis() {
        return tataTulis;
    }

    public void setTataTulis(KriteriaPenilaianSkripsi tataTulis) {
        this.tataTulis = tataTulis;
    }

    public KriteriaPenilaianSkripsi getKelengkapanMateri() {
        return kelengkapanMateri;
    }

    public void setKelengkapanMateri(KriteriaPenilaianSkripsi kelengkapanMateri) {
        this.kelengkapanMateri = kelengkapanMateri;
    }

    public KriteriaPenilaianSkripsi getPoster() {
        return poster;
    }

    public void setPoster(KriteriaPenilaianSkripsi poster) {
        this.poster = poster;
    }

    public KriteriaPenilaianSkripsi getPenguasaanMateri() {
        return penguasaanMateri;
    }

    public void setPenguasaanMateri(KriteriaPenilaianSkripsi penguasaanMateri) {
        this.penguasaanMateri = penguasaanMateri;
    }

    public KriteriaPenilaianSkripsi getProsesBimbingan() {
        return prosesBimbingan;
    }

    public void setProsesBimbingan(KriteriaPenilaianSkripsi prosesBimbingan) {
        this.prosesBimbingan = prosesBimbingan;
    }
}
