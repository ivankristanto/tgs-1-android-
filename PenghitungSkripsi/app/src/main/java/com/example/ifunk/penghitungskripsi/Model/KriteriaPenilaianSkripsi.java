package com.example.ifunk.penghitungskripsi.Model;

/**
 * Kelas KriteriaPeniliaianSkripsi
 */
public class KriteriaPenilaianSkripsi {

    private String namaKriteria;
    private float bobotNilai;
    private float nilai;

    public KriteriaPenilaianSkripsi(String namaKriteria, float bobotNilai) {
        this.namaKriteria = namaKriteria;
        this.bobotNilai = bobotNilai;
        this.nilai = 0f;
    }

    /**
     * Method untuk menghitung nilai kriteria berdasarkan nilai yang
     * dimasukan dengan bobot nilai kriteria
     * @param nilaiMasukan float
     */
    public void hitungNilai(float nilaiMasukan){
        this.nilai = nilaiMasukan*this.bobotNilai;
    }

    //Setter & Getter
    public float getNilai() {
        return nilai;
    }

    public void setNilai(float nilai) {
        this.nilai = nilai;
    }

    public void setNamaKriteria(String namaKriteria) {
        this.namaKriteria = namaKriteria;
    }

    public String getNamaKriteria() {
        return namaKriteria;
    }

    public void setBobotNilai(float bobotNilai){
        this.bobotNilai=bobotNilai;
    }

    public float getBobotNilai() {
        return bobotNilai;
    }
}
