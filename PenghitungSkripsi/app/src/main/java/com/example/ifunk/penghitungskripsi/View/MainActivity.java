package com.example.ifunk.penghitungskripsi.View;

import android.inputmethodservice.KeyboardView;
import android.media.MediaDrm;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.ifunk.penghitungskripsi.Presenter.MainPresenter;
import com.example.ifunk.penghitungskripsi.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener ,View.OnFocusChangeListener{

    //edit text
    protected EditText etTataTulis;
    protected EditText etKelengkapanMateri;
    protected EditText etPoster;
    protected EditText etPenguasaanMateri;
    protected EditText etProsesBimbingan;

    //text view bobot nilai
    protected TextView tvBobotNilaiTataTulis;
    protected TextView tvBobotNilaiKelengkapanMateri;
    protected TextView tvBobotNilaiPoster;
    protected TextView tvBobotNilaiPenguasaanMateri;
    protected TextView tvBobotNilaiProsesBimbingan;

    //text view nilai
    protected TextView tvNilaiTataTulis;
    protected TextView tvNilaiKelengkapanMateri;
    protected TextView tvNilaiPoster;
    protected TextView tvNilaiPenguasaanMateri;
    protected TextView tvNilaiProsesBimbingan;

    //text view nama
    protected TextView tvTataTulis;
    protected TextView tvKelengkapanMateri;
    protected TextView tvPoster;
    protected TextView tvPenguasaanMateri;
    protected TextView tvProsesBimbingan;

    //button
    protected Button btnHitung;

    //text view total nilai
    protected TextView nilaiTotal;

    //Presenter
    protected MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //text view edit text
        this.etTataTulis = this.findViewById(R.id.etTataTulis);
        this.etKelengkapanMateri = this.findViewById(R.id.etKelengkapanMateri);
        this.etPoster = this.findViewById(R.id.etPoster);
        this.etPenguasaanMateri = this.findViewById(R.id.etPenguasaanMateri);
        this.etProsesBimbingan = this.findViewById(R.id.etProsesBimbingan);

        //text view bobot nilai
        this.tvBobotNilaiTataTulis = this.findViewById(R.id.tvBobotNilaiTataTulis);
        this.tvBobotNilaiKelengkapanMateri = this.findViewById(R.id.tvBobotNilaiKelengkapanMateri);
        this.tvBobotNilaiPoster = this.findViewById(R.id.tvBobotNilaiPoster);
        this.tvBobotNilaiPenguasaanMateri = this.findViewById(R.id.tvBobotNilaiPenguasaanMateri);
        this.tvBobotNilaiProsesBimbingan = this.findViewById(R.id.tvBobotNilaiProsesBimbingan);

        //Text view hasil nilai
        this.tvNilaiTataTulis = this.findViewById(R.id.tvHasilNilaiTataTulis);
        this.tvNilaiKelengkapanMateri = this.findViewById(R.id.tvHasilNilaiKelengkapanMateri);
        this.tvNilaiPoster = this.findViewById(R.id.tvHasilNilaiPoster);
        this.tvNilaiPenguasaanMateri = this.findViewById(R.id.tvHasilNilaiPenguasaanMateri);
        this.tvNilaiProsesBimbingan = this.findViewById(R.id.tvHasilNilaiProsesBimbingan);

        //Text view nama
        this.tvTataTulis = this.findViewById(R.id.tvTataTulis);
        this.tvKelengkapanMateri = this.findViewById(R.id.tvKelengkapanMateri);
        this.tvPoster = this.findViewById(R.id.tvPoster);
        this.tvPenguasaanMateri = this.findViewById(R.id.tvPenguasaanMateri);
        this.tvProsesBimbingan = this.findViewById(R.id.tvProsesBimbingan);

        //Button
        this.btnHitung = this.findViewById(R.id.btnHitung);

        //total
        this.nilaiTotal = this.findViewById(R.id.nilaiTotal);

        //Presenter
        this.mainPresenter = new MainPresenter(this);

        //set listener
        this.btnHitung.setOnClickListener(this);
        this.etTataTulis.setOnFocusChangeListener(this);
        this.etKelengkapanMateri.setOnFocusChangeListener(this);
        this.etPoster.setOnFocusChangeListener(this);
        this.etPenguasaanMateri.setOnFocusChangeListener(this);
        this.etProsesBimbingan.setOnFocusChangeListener(this);

        //main presenter me-set semua view dari data di model
        this.mainPresenter.appStart();
    }



    @Override
    public void onClick(View view) {
        if (view.getId() == this.btnHitung.getId()){
            this.mainPresenter.onHitungButtonClicked();
        }
    }

    //GETTER
    public TextView getNilaiTotal() {
        return nilaiTotal;
    }

    public EditText getEtTataTulis() {
        return etTataTulis;
    }

    public EditText getEtKelengkapanMateri() {
        return etKelengkapanMateri;
    }

    public EditText getEtPoster() {
        return etPoster;
    }

    public EditText getEtPenguasaanMateri() {
        return etPenguasaanMateri;
    }

    public EditText getEtProsesBimbingan() {
        return etProsesBimbingan;
    }

    public TextView getTvBobotNilaiTataTulis() {
        return tvBobotNilaiTataTulis;
    }

    public TextView getTvBobotNilaiKelengkapanMateri() {
        return tvBobotNilaiKelengkapanMateri;
    }

    public TextView getTvBobotNilaiPoster() {
        return tvBobotNilaiPoster;
    }

    public TextView getTvBobotNilaiPenguasaanMateri() {
        return tvBobotNilaiPenguasaanMateri;
    }

    public TextView getTvBobotNilaiProsesBimbingan() {
        return tvBobotNilaiProsesBimbingan;
    }

    public TextView getTvNilaiTataTulis() {
        return tvNilaiTataTulis;
    }

    public TextView getTvNilaiKelengkapanMateri() {
        return tvNilaiKelengkapanMateri;
    }

    public TextView getTvNilaiPoster() {
        return tvNilaiPoster;
    }

    public TextView getTvNilaiPenguasaanMateri() {
        return tvNilaiPenguasaanMateri;
    }

    public TextView getTvNilaiProsesBimbingan() {
        return tvNilaiProsesBimbingan;
    }

    public TextView getTvTataTulis() {
        return tvTataTulis;
    }

    public TextView getTvKelengkapanMateri() {
        return tvKelengkapanMateri;
    }

    public TextView getTvPoster() {
        return tvPoster;
    }

    public TextView getTvPenguasaanMateri() {
        return tvPenguasaanMateri;
    }

    public TextView getTvProsesBimbingan() {
        return tvProsesBimbingan;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        this.mainPresenter.onHitungButtonClicked();
    }
}
