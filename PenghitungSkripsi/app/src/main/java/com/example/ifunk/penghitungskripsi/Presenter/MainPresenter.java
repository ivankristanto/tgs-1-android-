package com.example.ifunk.penghitungskripsi.Presenter;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.example.ifunk.penghitungskripsi.Model.KriteriaPenilaianSkripsi;
import com.example.ifunk.penghitungskripsi.Model.KumpulanKriteria;
import com.example.ifunk.penghitungskripsi.R;
import com.example.ifunk.penghitungskripsi.View.MainActivity;

public class MainPresenter {
    private KumpulanKriteria kumpulanKriteria;
    private MainActivity main;

    public MainPresenter(MainActivity main) {
        this.main = main;
        this.kumpulanKriteria = new KumpulanKriteria();

//        Drawable d= this.main.getEtPoster().getBackground();
//        d.setColorFilter(Color.rgb(200,0,0), PorterDuff.Mode.DST_ATOP);
//        this.main.getEtPoster().setBackground(d);
    }

    public void appStart(){
        //Edit text
        this.main.getEtTataTulis().setText("");
        this.main.getEtKelengkapanMateri().setText("");
        this.main.getEtPenguasaanMateri().setText("");
        this.main.getEtPoster().setText("");
        this.main.getEtProsesBimbingan().setText("");

        //Text view bobot nilai
        this.main.getTvBobotNilaiTataTulis().setText(String.format("%3.0f",this.kumpulanKriteria.getTataTulis().getBobotNilai()*100)+"%");
        this.main.getTvBobotNilaiKelengkapanMateri().setText(String.format("%3.0f",this.kumpulanKriteria.getKelengkapanMateri().getBobotNilai()*100)+"%");
        this.main.getTvBobotNilaiPenguasaanMateri().setText(String.format("%3.0f",this.kumpulanKriteria.getPenguasaanMateri().getBobotNilai()*100)+"%");
        this.main.getTvBobotNilaiPoster().setText(String.format("%3.0f",this.kumpulanKriteria.getPoster().getBobotNilai()*100)+"%");
        this.main.getTvBobotNilaiProsesBimbingan().setText(String.format("%3.0f",this.kumpulanKriteria.getProsesBimbingan().getBobotNilai()*100)+"%");

        //Text view nama
        this.main.getTvTataTulis().setText(this.kumpulanKriteria.getTataTulis().getNamaKriteria());
        this.main.getTvPenguasaanMateri().setText(this.kumpulanKriteria.getPenguasaanMateri().getNamaKriteria());
        this.main.getTvPoster().setText(this.kumpulanKriteria.getPoster().getNamaKriteria());
        this.main.getTvKelengkapanMateri().setText(this.kumpulanKriteria.getKelengkapanMateri().getNamaKriteria());
        this.main.getTvProsesBimbingan().setText(this.kumpulanKriteria.getProsesBimbingan().getNamaKriteria());
    }

    /**
     * method untuk menghitung total nilai saat 'hitung' button di-click
     */
    public void onHitungButtonClicked(){
        //HITUNG TATATULIS
        String etTataTulis = this.main.getEtTataTulis().getText()+"";
        if (!etTataTulis.equalsIgnoreCase(""))
        {
            this.main.getEtTataTulis().setBackgroundColor(Color.rgb(0,200,0));
            if (Integer.parseInt(etTataTulis) > 100){
                Log.d("ERROR INPUT","--> EditText TataTulis : Input > 100 , set text EditText to 100");
                this.main.getEtTataTulis().setText("100");
                etTataTulis = "100";
            } else if(Integer.parseInt(etTataTulis) < 0){
                Log.d("ERROR INPUT","--> EditText TataTulis : Input < 0 , set text EditText to 0");
                this.main.getEtTataTulis().setText("0");
                etTataTulis = "0";
            }
            this.kumpulanKriteria.getTataTulis().hitungNilai(Float.parseFloat(etTataTulis));
            this.main.getTvNilaiTataTulis().setText(String.format("%3.2f",this.kumpulanKriteria.getTataTulis().getNilai()));
        }else{
            Log.d("ERROR INPUT","--> EditText TataTulis : Input is empty!");
            this.main.getEtTataTulis().setBackgroundColor(Color.rgb(200,0,0));
        }

        //HITUNG KELENGKAPAN MATERI
        String etKelengkapanMateri = this.main.getEtKelengkapanMateri().getText()+"";
        if (!etKelengkapanMateri.equalsIgnoreCase(""))
        {
            this.main.getEtKelengkapanMateri().setBackgroundColor(Color.rgb(0,200,0));
            if (Integer.parseInt(etKelengkapanMateri) > 100){
                Log.d("ERROR INPUT","--> EditText KelengkapanMateri : Input > 100 , set text EditText to 100");
                this.main.getEtKelengkapanMateri().setText("100");
                etKelengkapanMateri = "100";
            } else if(Integer.parseInt(etKelengkapanMateri) < 0){
                Log.d("ERROR INPUT","--> EditText KelengkapanMateri : Input < 0 , set text EditText to 0");
                this.main.getEtKelengkapanMateri().setText("0");
                etKelengkapanMateri = "0";
            }
            this.kumpulanKriteria.getKelengkapanMateri().hitungNilai(Float.parseFloat(etKelengkapanMateri));
            this.main.getTvNilaiKelengkapanMateri().setText(String.format("%3.2f",this.kumpulanKriteria.getKelengkapanMateri().getNilai()));
        }else{
            Log.d("ERROR INPUT","--> EditText KelengkapanMateri : Input is empty!");
            this.main.getEtKelengkapanMateri().setBackgroundColor(Color.rgb(200,0,0));
        }

        //HITUNG POSTER
        String etPoster = this.main.getEtPoster().getText()+"";
        if (!etPoster.equalsIgnoreCase(""))
        {
            this.main.getEtPoster().setBackgroundColor(Color.rgb(0,200,0));
            if (Integer.parseInt(etPoster) > 100){
                Log.d("ERROR INPUT","--> EditText Poster : Input > 100 , set text EditText to 100");
                this.main.getEtPoster().setText("100");
                etPoster = "100";
            } else if(Integer.parseInt(etPoster) < 0){
                Log.d("ERROR INPUT","--> EditText Poster : Input < 0 , set text EditText to 0");
                this.main.getEtPoster().setText("0");
                etPoster = "0";
            }
            this.kumpulanKriteria.getPoster().hitungNilai(Float.parseFloat(etPoster));
            this.main.getTvNilaiPoster().setText(String.format("%3.2f",this.kumpulanKriteria.getPoster().getNilai()));
        }else{
            Log.d("ERROR INPUT","--> EditText Poster : Input is empty!");
            this.main.getEtPoster().setBackgroundColor(Color.rgb(200,0,0));
        }

        //HITUNG PENGUASAAN MATERI
        String etPenguasanMateri = this.main.getEtPenguasaanMateri().getText()+"";
        if (!etPenguasanMateri.equalsIgnoreCase(""))
        {
            this.main.getEtPenguasaanMateri().setBackgroundColor(Color.rgb(0,200,0));
            if (Integer.parseInt(etPenguasanMateri) > 100){
                Log.d("ERROR INPUT","--> EditText PenguasaanMateri : Input > 100 , set text EditText to 100");
                this.main.getEtPenguasaanMateri().setText("100");
                etPenguasanMateri = "100";
            } else if(Integer.parseInt(etPenguasanMateri) < 0){
                Log.d("ERROR INPUT","--> EditText PenguasaanMateri : Input < 0 , set text EditText to 0");
                this.main.getEtPenguasaanMateri().setText("0");
                etPenguasanMateri = "0";
            }
            this.kumpulanKriteria.getPenguasaanMateri().hitungNilai(Float.parseFloat(etPenguasanMateri));
            this.main.getTvNilaiPenguasaanMateri().setText(String.format("%3.2f",this.kumpulanKriteria.getPenguasaanMateri().getNilai()));
        }else{
            Log.d("ERROR INPUT","--> EditText PenguasaanMateri : Input is empty!");
            this.main.getEtPenguasaanMateri().setBackgroundColor(Color.rgb(200,0,0));
        }

        //HITUNG PROSES BIMBINGAN
        String etProsesBimbingan = this.main.getEtProsesBimbingan().getText()+"";
        if (!etProsesBimbingan.equalsIgnoreCase(""))
        {
            this.main.getEtProsesBimbingan().setBackgroundColor(Color.rgb(0,200,0));
            if (Integer.parseInt(etProsesBimbingan) > 100){
                Log.d("ERROR INPUT","--> EditText ProsesBimbingan : Input > 100 , set text EditText to 100");
                this.main.getEtProsesBimbingan().setText("100");
                etProsesBimbingan = "100";
            } else if(Integer.parseInt(etProsesBimbingan) < 0){
                Log.d("ERROR INPUT","--> EditText ProsesBimbingan : Input < 0 , set text EditText to 0");
                this.main.getEtProsesBimbingan().setText("0");
                etProsesBimbingan = "0";
            }
            this.kumpulanKriteria.getProsesBimbingan().hitungNilai(Float.parseFloat(etProsesBimbingan));
            this.main.getTvNilaiProsesBimbingan().setText(String.format("%3.2f",this.kumpulanKriteria.getProsesBimbingan().getNilai()));
        }else{
            Log.d("ERROR INPUT","--> EditText ProsesBimbingan : Input is empty!");
            this.main.getEtProsesBimbingan().setBackgroundColor(Color.rgb(200,0,0));
        }

        //HITUNG TOTAL NILAI
        this.kumpulanKriteria.hitungTotalNilai();
        this.main.getNilaiTotal().setText(String.format("%3.2f",this.kumpulanKriteria.getTotalNilai()));
        Log.d("OUTPUT SUCCESS","--> TextView Total nilai updated!");
    }


}
